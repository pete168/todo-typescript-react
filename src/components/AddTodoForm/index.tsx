import React, { useState, ChangeEvent, FormEvent } from "react";

import "./style.css";

interface AddTodoFormProps {
  addTodo: AddTodo;
}

const AddTodoForm: React.FC<AddTodoFormProps> = ({ addTodo }) => {
  const [newTodo, setNewTodo] = useState("");

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setNewTodo(e.target.value);
  };

  const handleSubmit = (e: FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    addTodo(newTodo);
    setNewTodo("");
  };

  return (
    <form>
      <input
        type="text"
        value={newTodo}
        placeholder="Add new task"
        onChange={handleChange}
      />
      <button type="submit" onClick={handleSubmit}>
        Add todo
      </button>
    </form>
  );
};

export default AddTodoForm;
