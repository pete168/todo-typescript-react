import React from "react";
import TodoListItem from "../TodoListItem";

import "./style.css";

interface TodoListProps {
  todos: Array<Todo>;
  toggleTodo: ToggleTodo;
}

const TodoList: React.FC<TodoListProps> = ({ todos, toggleTodo }) => {
  return (
    <>
      <span>Your Tasks:</span>
      <ul>
        {todos.map((todo, i) => {
          return <TodoListItem key={i} todo={todo} toggleTodo={toggleTodo} />;
        })}
      </ul>
    </>
  );
};

export default TodoList;
